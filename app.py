from Tkinter import *
from math import sqrt
from random import randint
class veztempl:
    def __init__(self, rang, dmg, firerate, cost, buildtime, fat, color, firecolor):
        self.cost = cost
        self.rang = rang
        self.dmg = dmg
        self.firerate = firerate
        self.color = color
        self.firecolor = firecolor
        self.fat = fat
        self.buildtime = buildtime

class recyct:
    def __init__(self):
        self.cost = 250
        self.rang = 30
        self.fat = 30
        self.firerate = None
        self.dmg = None
        self.color = PhotoImage(file="rectower.gif")
        self.firecolor = None
        self.buildtime = 200


class App:
    #todo, workers
    #todo, ovladani mysi, waves
    def __init__(self):
        self.map = [100, 200, 180, 180, 200, 250, 260, 230, 300, 380, 150, 360, 140, 460, 350, 450, 400, 70, 320, 60, 330, 150]
        self.okno = Tk()
        self.canvas = Canvas(self.okno, width=500, height=500, bg="#FFFA5E")
        self.okno.bind("<Button-1>", self.klik)
        self.okno.bind("<Key>", self.sett)
        self.okno.bind("<Motion>", self.hejb)
        self.canvas.pack()
        self.veze = []
        self.cs = []
        self.redimg = PhotoImage(file="redtower.gif")
        self.sniimg = PhotoImage(file="snitower.gif")
        self.plaimg = PhotoImage(file="platower.gif")
        # rang, dmg, firerate, cost, time, fat, color, firecolor
        self.r = veztempl(50, 40, 4, 120, 120, 15, self.redimg, "#ffbbbb")
        self.w = veztempl(100, 5, 5, 60, 70, 15, "White", "#dddddd")
        self.s = veztempl(300, 150, 100, 200, 200, 15, self.sniimg, "#000000")
        self.m = veztempl(90, 100, 5, 1500, 1000, 25, self.plaimg, "#0000ff")
        self.c = recyct()
        self.selection = self.w
        self.enemies = []
        self.deade = []
        self.prachy = 2000
        self.wave = Wave(self)
        self.canvas.create_line(self.map, width=30, fill="#aaaaaa")  # udela cestu
        self.rangeoval = self.canvas.create_oval(1000, 1000, 1000, 1000,)
        self.buildoval = self.canvas.create_oval(1000, 1000, 1000, 1000, fill="#888888")
        self.workers = [Worker(self), Worker(self)]
        self.prachytext = self.canvas.create_text(450, 20, text=str(self.prachy), font="Arial 30")
        self.obdly = []
        for i in range(0, len(self.map)-2, 2):  # vytvoreni obdlu pro kontrolovani prekrejvani cesty
            c = []
            a = (self.map[i]-self.map[i+2])*30/(sqrt((self.map[i]-self.map[i+2])**2+(self.map[i+1]-self.map[i+3])**2))
            b = (self.map[i+1]-self.map[i+3])*30/(sqrt((self.map[i]-self.map[i+2])**2+(self.map[i+1]-self.map[i+3])**2))
            c.append([self.map[i]-b, self.map[i+1]+a])
            c.append([self.map[i]+b, self.map[i+1]-a])
            c.append([self.map[i+2]+b, self.map[i+3]-a])
            c.append([self.map[i+2]-b, self.map[i+3]+a])
            self.obdly.append(c)
        self.vykresli()
        self.pohyb()

    def hejb(self, event):
        self.k = True
        for i in self.veze + self.cs:
            if sqrt((i.x-event.x)**2+(i.y-event.y)**2) < self.selection.fat + i.fat:
                self.k = False
        for i in range(len(self.map)/2):
            if sqrt((self.map[i*2]-event.x)**2+(self.map[i*2+1]-event.y)**2) < self.selection.fat + 15:
                self.k = False
        p = self.selection.rang
        q = self.selection.fat
        self.canvas.coords(self.rangeoval, event.x-p, event.y-p, event.x+p, event.y+p)
        self.canvas.coords(self.buildoval, event.x-q, event.y-q, event.x+q, event.y+q)
        for c in self.obdly:
            growth = (c[0][1] - c[1][1]) / (c[0][0] - c[1][0])
            vyska = (event.x - c[0][0]) * growth + c[0][1]
            vyska2 = (event.x - c[2][0]) * growth + c[2][1]
            if (event.y > vyska2 and event.y < vyska) or (event.y < vyska2 and event.y > vyska):
                growth = (c[0][0] - c[3][0]) / (c[0][1] - c[3][1])
                vyska = (event.y - c[0][1]) * growth + c[0][0]
                vyska2 = (event.y - c[2][1]) * growth + c[2][0]
                if (event.x > vyska2 and event.x < vyska) or (event.x < vyska2 and event.x > vyska):
                    self.k = False
        if self.k:
            self.canvas.itemconfig(self.buildoval, fill="#888888")
        if not self.k:
            self.canvas.itemconfig(self.buildoval, fill="#ff8888")

    def pohyb(self):
        '''hlavni smycka, klasika'''
        for i in self.veze + self.enemies + [self.wave] + self.workers:
            i.frame()
        self.canvas.itemconfig(self.prachytext, text=str(self.prachy))
        self.okno.after(20, self.pohyb)


    def vykresli(self):
        for i in self.veze:
            i.vykresli1()
        for i in self.enemies:
            i.vykresli()
    
    def sett(self, event):
        if event.char == "r":
            self.udelobdly(30)
            self.selection = self.r
        elif event.char == "w":
            self.udelobdly(30)
            self.selection = self.w
        elif event.char == "s":
            self.udelobdly(30)
            self.selection = self.s
        elif event.char == "c":
            self.udelobdly(45)
            self.selection = self.c
        elif event.char == "m":
	    self.udelobdly(40)
	    self.selection = self.m

    def udelobdly(self, dist):
        self.obdly = []
        for i in range(0, len(self.map)-2, 2):  # vytvoreni obdlu pro kontrolovani prekrejvani cesty
            c = []
            a = (self.map[i]-self.map[i+2])*dist/(sqrt((self.map[i]-self.map[i+2])**2+(self.map[i+1]-self.map[i+3])**2))
            b = (self.map[i+1]-self.map[i+3])*dist/(sqrt((self.map[i]-self.map[i+2])**2+(self.map[i+1]-self.map[i+3])**2))
            c.append([self.map[i]-b, self.map[i+1]+a])
            c.append([self.map[i]+b, self.map[i+1]-a])
            c.append([self.map[i+2]+b, self.map[i+3]-a])
            c.append([self.map[i+2]-b, self.map[i+3]+a])
            self.obdly.append(c)
            
    def klik(self, event):
        if self.prachy >= self.selection.cost and self.k:
            self.prachy -= self.selection.cost
            if self.selection == self.c:
                return self.cs.append(Wvez(self, event.x, event.y, self.selection.rang, self.selection.firerate, self.selection.dmg, self.selection.color, self.selection.firecolor, self.selection.fat, self.selection.buildtime))
            self.veze.append(Wvez(self, event.x, event.y, self.selection.rang, self.selection.firerate, self.selection.dmg, self.selection.color, self.selection.firecolor, self.selection.fat, self.selection.buildtime))

class Worker:
    # k nejblizsi c, k mrtvolce jen jeden najednou
    def __init__(self, app):
        self.x = randint(100, 300)
        self.y = randint(100, 300)
        self.a = app
        self.speed = 1.5
        self.deade = None
        self.deadcand = None
        self.oval = self.a.canvas.create_oval(self.x-10, self.y-10, self.x+10, self.y+10, fill="magenta")

    def hejbk(self, x, y):
        c = sqrt((self.center()[0]-x)**2+(self.center()[1]-y)**2)
        r = self.speed/c
        self.a.canvas.move(self.oval, (x-self.center()[0])*r,  (y-self.center()[1])*r)
        
    def frame(self):
        if self.deade == None:  # kdyz nema mrtvolu, jde k mrtvole
            if len(self.a.deade) > 0:
	        if not self.deadcand in self.a.deade:
		    self.deadcand = self.a.deade[0]
		    for i in self.a.deade:
			if sqrt((i.center()[0]-self.center()[0])**2 + (i.center()[1]-self.center()[1])**2) < sqrt((self.deadcand.center()[0]-self.center()[0])**2 + (self.deadcand.center()[1]-self.center()[1])**2):
			    self.deadcand = i
		
		else:
		    self.hejbk(self.deadcand.center()[0], self.deadcand.center()[1])
		    if abs(self.center()[0] - self.deadcand.center()[0]) < self.speed and abs(self.center()[1] - self.deadcand.center()[1]) < self.speed:
			self.deade = self.deadcand
			self.a.deade.remove(self.deadcand)
			self.candidate = None
        elif len(self.a.cs) > 0:  # kdyz dojde k mrtvole, najde nejblizsi rec cent
            self.deade.speed = self.speed
            if self.candidate == None:
                self.candidate = self.a.cs[0]
                for i in self.a.cs:
                    if sqrt((i.center()[0] - self.center()[0])**2 + (i.center()[1] - self.center()[1])**2) < sqrt((self.candidate.center()[0] - self.center()[0])**2 + (self.candidate.center()[1] - self.center()[1])**2):
                        self.candidate = i
            else:  # ma cil a hejbe se
                self.hejbk(self.candidate.center()[0], self.candidate.center()[1])
                self.deade.hejbk(self.candidate.center()[0], self.candidate.center()[1])
                if abs(self.center()[0] - self.candidate.center()[0]) < self.speed and abs(self.center()[1] - self.candidate.center()[1]) < self.speed:
		    # donesl do centra
		    self.a.prachy = int(self.a.prachy + self.deade.bounty*1.5)
		    self.target = None
		    self.a.canvas.delete(self.deade.oval, self.deade.red, self.deade.green)
		    self.deade = None
		    self.deadcand = None
    def center(self):
        return [self.a.canvas.coords(self.oval)[0]+10, self.a.canvas.coords(self.oval)[1]+10]

class Vez:
    '''Zaklad pro spesl veze - udelam to jako instadmg - zadny litajici projektily, neefektivni dmg me fakt stve'''
    def __init__(self, app, x, y, ran, firerate, dmg, color, firecolor, fat, buildtime):
        self.x = x
        self.y = y
        self.fat = fat
        self.dmg = dmg
        self.firerate = firerate
        self.ran = ran
        self.a = app
        self.fireanimtimer = 0.1
        self.dostrely = 0
        self.buildtime = buildtime
        self.color = color
        self.firecolor = firecolor
        self.vykresli1()

    def frame(self):
        self.fireanimtimer -= 1
        if self.fireanimtimer == 0:
            self.a.canvas.delete(self.fireline)
            self.fireanimtimer = 0.1
        if self.dostrely > 0:
            self.dostrely -= 1
        else:
            self.vystrel()

    def vykresli1(self):
        self.oval2 = self.a.canvas.create_oval(self.x-self.fat, self.y-self.fat, self.x+self.fat, self.y+self.fat)
        self.oval = self.a.canvas.create_image(self.x, self.y, image=self.color)
        self.green = self.a.canvas.create_rectangle(self.a.canvas.coords(self.oval)[0]-5, self.a.canvas.coords(self.oval)[1]-5, self.a.canvas.coords(self.oval)[0]+25, self.a.canvas.coords(self.oval)[1]-3, fill="green")
        self.red = self.a.canvas.create_rectangle(self.a.canvas.coords(self.oval)[0]-5, self.a.canvas.coords(self.oval)[1]-5, self.a.canvas.coords(self.oval)[0]-5, self.a.canvas.coords(self.oval)[1]-3, fill="red")


    def upravtime(self):
        self.a.canvas.coords(self.green, self.a.canvas.coords(self.oval)[0]-5, self.a.canvas.coords(self.oval)[1]-5, self.a.canvas.coords(self.oval)[0]+25, self.a.canvas.coords(self.oval)[1]-3)
        self.a.canvas.coords(self.red, self.a.canvas.coords(self.oval)[0]-5, self.a.canvas.coords(self.oval)[1]-5, self.a.canvas.coords(self.oval)[0]+25-((self.hp+0.0)/self.maxhp)*30, self.a.canvas.coords(self.oval)[1]-3)

    def center(self):
        return [self.x, self.y]

#todo, vymyslet vic druhu vezi, enemaku
class Wvez(Vez):

    def vystrel(self):
        '''neznamena ze vystreli, i otestuje, '''
        #todo locking
        for i in self.a.enemies:
            dist = sqrt((i.center()[0] - self.x)**2 + (i.center()[1] - self.y)**2)
            if dist < self.ran and i.hp > 0:
                self.fireline = self.a.canvas.create_line(self.x, self.y, i.center(), width=5, fill=self.firecolor)
                self.fireanimtimer = 3
                self.dostrely = self.firerate
                i.dmg(self.dmg)
                return

class Wave:
    def __init__(self, app):
        self.a = app
        self.type = [self.a, 100, 5, 20]
        self.standarttime = 100
        self.timer = 100

    def frame(self):
        self.timer -= 1
        if self.timer == 0:
            self.type[1] *= 1.1
            self.type[3] = int(self.type[3]*1.1)
            self.a.enemies.append(Enemy(self.type[0], self.type[1], self.type[2], self.type[3]))
            self.standarttime *= 0.98
            self.timer = int(self.standarttime)


class Enemy:
    '''zaklad pro vsechny enemy'''
    # todo - pri prenosu workrama prodlouzeni hp baru (odstranit)
    def __init__(self, app, hp, speed, bounty):
        self.dead = False
        self.hp = hp
        self.bounty = bounty
        self.maxhp = hp
        self.mappos = 1
        self.speed = speed
        self.a = app
        self.oval = self.a.canvas.create_oval(self.a.map[0]-10, self.a.map[1]-10, self.a.map[0]+10, self.a.map[1]+10, fill="blue")
        self.green = self.a.canvas.create_rectangle(self.a.canvas.coords(self.oval)[0]-5, self.a.canvas.coords(self.oval)[1]-5, self.a.canvas.coords(self.oval)[0]+25, self.a.canvas.coords(self.oval)[1]-3, fill="green")
        self.red = self.a.canvas.create_rectangle(self.a.canvas.coords(self.oval)[0]-5, self.a.canvas.coords(self.oval)[1]-5, self.a.canvas.coords(self.oval)[0]-5, self.a.canvas.coords(self.oval)[1]-3, fill="red")

    def frame(self):
        if len(a.map) <= self.mappos*2:
            #print "cil"
            return
        if not self.hp <= 0:
            c = sqrt((self.center()[0]-self.a.map[self.mappos*2])**2+(self.center()[1]-self.a.map[self.mappos*2+1])**2)
            r = self.speed/c
            self.hejbni((self.a.map[self.mappos*2]-self.center()[0])*r,  (self.a.map[self.mappos*2+1]-self.center()[1])*r)
            if abs(self.center()[0] - self.a.map[self.mappos*2]) < self.speed and abs(self.center()[1] - self.a.map[self.mappos*2+1]) < self.speed:
                self.mappos += 1
        elif not self.dead:
            self.dead = True
            self.hp = 0
            self.a.deade.append(self)
            self.a.enemies.remove(self)
            self.a.prachy += self.bounty
            self.a.canvas.itemconfig(self.oval, fill="#7569FF")
            self.a.canvas.coords(self.red, self.a.canvas.coords(self.oval)[0]-5, self.a.canvas.coords(self.oval)[1]-5, self.a.canvas.coords(self.oval)[0]+25, self.a.canvas.coords(self.oval)[1]-3)
    
    def hejbni(self, x, y):
        self.a.canvas.move(self.oval, x, y)
        self.a.canvas.coords(self.green, self.a.canvas.coords(self.oval)[0]-5, self.a.canvas.coords(self.oval)[1]-5, self.a.canvas.coords(self.oval)[0]+25, self.a.canvas.coords(self.oval)[1]-3)
        self.a.canvas.coords(self.red, self.a.canvas.coords(self.oval)[0]-5, self.a.canvas.coords(self.oval)[1]-5, self.a.canvas.coords(self.oval)[0]+25-((self.hp+0.0)/self.maxhp)*30, self.a.canvas.coords(self.oval)[1]-3)

    def hejbk(self, x, y):
        c = sqrt((self.center()[0]-x)**2+(self.center()[1]-y)**2)
        r = self.speed/c
        self.hejbni((x-self.center()[0])*r, (y-self.center()[1])*r)
        
    def center(self):
        return [self.a.canvas.coords(self.oval)[0]+10, self.a.canvas.coords(self.oval)[1]+10]

    def dmg(self, stren):
        self.hp -= stren

a = App()
mainloop()